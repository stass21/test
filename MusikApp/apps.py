from django.apps import AppConfig


class MusikappConfig(AppConfig):
    name = 'MusikApp'
